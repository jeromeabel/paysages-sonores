# Atelier Jeux & Paysages Sonores

## Objectifs
- Découvertes des sons de l'environnement sonore (sons concrets) : écoutes/observation, enregistrements
- Jeux & interactions avec les sons : déclenchements, rythmes, effets sonores (délais, reverb, vitesse, panoramique)
- Jeux en public (dehors sous les arbres quand il fait beau ou dans une salle)

## Animateur
Jérôme Abel, artiste plasticien & multimédia

## Logistique
- Public : Patients de l'hôpital Marius Lacroix.
- Nombre de participants : environ 6 personnes
- Matériel : 
	- Système sonore : 2 enceintes et un caisson de basse
	- Rallonge / multiprise
	- Ordinateur
	- Cartes électroniques et capteurs
	- Carte son
	- Enregistrement sonore
	- Tables
	- Chaises, transat, coussins

## Notes sur la prise de sons
- Attention en tenant l'appareil (bruits parasites)
- Attention au kway quand on marche ?
- Trop prêt = saturation
- Codes pour que personne ne parle
- Réécouter les sons
- Bruits répétitifs > laisser finir les bruits

## Atelier #1 -2020

- Patients : 4 adultes
- Référents : Élise
- Dates :  8, 11, 15 et 18 décembre 2020
- Horaires : 13h30 - 15h30 (temps de préparation 30 min)
- Capteurs utilisés : 
	- 5 objets en métal (capacitif) + 2 potentiomètres
	- 1 capteur crayon
	- 4 glissières + 4 boutons + 2 lumières

**Session 1**
- Découverte des interactions, des sons (synthèses, concrets, voix)

**Session 2**
- Enregistrement sonore, ballade dans l'hôpital vers des points intéressants : cuisine, cafétéria, animaux, pluie, moteurs, klaxons, alarmes, porte clefs, jardinier, ...

**Session 3**
- Écoute des sons enregistrés et classés, classement. Composition collective (post-it sur le mur) et tests sur les sons.

**Session 4**
- Affinage des compositions. Jeux deux par deux en vue d'une présentation devant un petit public (ou pas si cela pose problème), dans la cafétéria ou dans la grande salle. Enregistrement des compos.


## Atelier #2 - 2021
- Patients : 5 jeunes autistes, 8-10 ans
- Référente : Sonia
- Dates : 27 avril, 4 mai, 11 mai 2021
- Horaires : 14h - 15h (temps de préparation 30 min)
- Capteurs utilisés : 
	- 4 bâtons en bois de toucher (capacitif)
	- 1 potentiomètre
	- 1 bouton
	- 1 pression
	- 2 lumières

**Session 1**
- Interactions & sons (synthèses, concrets, voix). Découverte des objets capteurs (cubes noirs). Écouter comment les sons sont déclenchés ou modfiés. Tests avec un capteur, puis deux.

**Session 2**
- Capteurs de lumières dans le noir. Deux jeunes commencent à trouver leur marque, un type de capteur qu'ils apprécient.

**Session 3**
- Vraie sonorisation. Jeux à plusieurs (harmoniques de guitare), lumières et bruits, etc. Avec une chaise les jeunes un peu en retrait ont trouvé leur marque. Chacun a pû s'approprier un type de capteur et le son associé.

**Pistes non testées**
- Travail possible sur le souffle pour certains
- Travail avec des images associées aux sons : eau, porte, métal, repas, maison, temps calme, goûter, jeux dedans, jeux dehors, ...


## Atelier #3 -2022
- Lieu : Embellie
- Patients : 4 adultes
- Référents : Anne-Sophie, Sébastien
- Dates :  21 janvier (annulé), 28 janvier, 4 et 11 février 2022 
- Horaires : 10h - 12h (+temps de préparation 20 min)


**Session 1**
- découvertes des outils, jeux sonores
- ballade sonore de prises de sons

**Session 2**
- jeux avec sons de la ballade > composition

**Session 3**
- jeux et composition finale
- enregistrements


## Technique
Patch Pure Data avec La Malinette + else.
Samples : http://jeromeabel.net/dev/marius.zip


