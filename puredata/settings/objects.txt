in: arduino-in arduino-in-num audio-in joystick joystick-win key-char key-makey key-num m-midi-in midi-pad mouse osc-in video-camera;

numbers: @-mem average bangs between bpm-ms chance count data-sampler fade flow folder for g-editor g g-play g-sampler inverse logic map-auto m map mem midi-hz midi-sampler multimeter notescale onoff oscilloscope physics rand-bangs rand-del rand rand-n rand-range rand-sel rand-walk start serializer taptempo;

seq: chrono seq-count seq-count2 seq-master seq-nb seq-pulse seq16 seq16-count seq16-nb seq16x3 seq24 seq24-count seq24-nb seq24x6;

audio: audio-attack audio-bank audio-chorus audio-comp audio-conv audio-delay audio-delay4 audio-disto audio-eq13 audio-eq3 audio-filter audio-flanger audio-formant audio-freeze audio-freezeverb audio-in audio-lowfi audio-map audio-mix audio-moog audio-nb audio-out audio-pan audio-phaser audio-pitch audio-rec-file audio-rec audio-reverb audio-sampler audio-vocoder audio-vol crash fftscope~ hat kick oscilloscope-big~ oscilloscope~ out~ snare synth synth~ synth-drumbass synth-drums synth-emu synth-fm synth-kick synth-juno synth-material synth-pluck synth-wobble synth-xylo;

video: track-areas track-color track-fiducials track-motion video-camera video-color video-crop video-effects video-eye video-graph video-halftone video-images video-kalei video-larsen video-offset video-mix video-object video-out video-rec video-sampler video-text video-xyz;

out: arduino-out audio-out dmx-out m-midi-out midi-out osc-out video-graph video-object video-out video-text;

master: audio-master arduino-master video-master;

bb: drums drums-machine fx-delay fx-disto fx-pitch fx-reverb fx-tremolo fx-wah sampler sequencer synth-bass synth-classic synth-fly synth-lead synth-noise synth-organ synth-percussion synth-shortbass synth-xylophone;
