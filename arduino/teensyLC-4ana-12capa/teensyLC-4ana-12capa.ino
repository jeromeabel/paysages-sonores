/* 
 * TEENSY LC CAPA for BRUTBOX
 * 12 capacitive inputs (via i2C mpr121) + 4 analog sensors send MIDI messages
 * 
 * Wiring :
 * Teensy       MPR121 board
 * 3V(=3.3V)    3.3V
 * GND          GND
 * 18 SDA       SDA
 * 19 SCL       SCL
 *
 * Licence : GNU/GPL3
 * Date : 2021
 *
 * Midi mapping : 
 * channel 2
 * 12 capacitive sensors : midi controller 1
 * 4 analog sensors : midi controller from 2 to 5
 */

#include <Wire.h>
#include "Adafruit_MPR121.h"

#ifndef _BV
#define _BV(bit) (1 << (bit)) 
#endif

// Global setup
const int MIDI_CHANNEL = 2;
const int SAMPLING_RATE = 20; // in msec
const int CAPA_NB = 6; // number of capa inputs
const int ANA_NB = 2; // number of ana inputs

// Analog setup
int anaPins[] = { 14, 15, 16, 17 }; // analog pins
int anaCurrentValues[ANA_NB]; // current analog values
int anaPreviousValues[ANA_NB]; // previous analog values

// Capa setup
Adafruit_MPR121 cap = Adafruit_MPR121();
uint16_t lasttouched = 0; // Keeps track of the last pins touched
uint16_t currtouched = 0; // so we know when buttons are 'released'
 
// Sampling rate
elapsedMillis msec = 0;

void setup() { 
  // Default address is 0x5A, if tied to 3.3V its 0x5B
  if (!cap.begin(0x5A)) {
    while (1);
  }
} 
 
void loop() {
  if (msec >= SAMPLING_RATE) {
    msec = 0;
    
    // Read analog values
    for (int i = 0; i < ANA_NB; i++) {
      anaCurrentValues[i] = (int) analogRead(anaPins[i])  / 8 ;
    }

    // Then send MIDI Control Change message
    for (int i = 0; i < ANA_NB; i++) {
      if (anaCurrentValues[i] != anaPreviousValues[i]) { // Check if current value is different from previous one
        usbMIDI.sendControlChange(i+2, anaCurrentValues[i], MIDI_CHANNEL);
        anaPreviousValues[i] = anaCurrentValues[i];
      }
    }
    
    // Get the currently touched pads
    currtouched = cap.touched();
  
    for (uint8_t i = 0; i < CAPA_NB; i++) {
      // it if *is* touched and *wasnt* touched before, alert!
      if ((currtouched & _BV(i)) && !(lasttouched & _BV(i)) ) {
        usbMIDI.sendControlChange(1, i, MIDI_CHANNEL);
        }
      }
       
    // Reset our state
    lasttouched = currtouched;
  }
 
  while (usbMIDI.read()) {
    // Discard incoming MIDI messages.
  }
}
