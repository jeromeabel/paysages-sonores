/* 
 * TEENSY 2 for BRUTBOX
 * 8 Analog sensors to Midi control change
 *
 * Licence : GNU/GPL3
 * Date : 2015 (revision 2021)
 * Wiki : http://reso-nance.org/wiki/projets/malinette-brutbox
 *
 * Midi mapping :
 * 8 analog sensors : midi controller from 1 to 8
 * Teensy can get input from 12 analog sensors
 */
 
 
// Global setup
const int MIDI_CHANNEL = 1;
const int SAMPLING_RATE = 40; // in msec
const int ANA_NB = 8; // number of inputs

// Analog setup
int anaPins[] = { 14, 15, 16, 17, 18, 19, 20, 21, 13, 12, 11, 22 }; // analog pins
int anaCurrentValues[ANA_NB]; // current analog values
int anaPreviousValues[ANA_NB]; // previous analog values
 
// Sampling rate
elapsedMillis msec = 0;

void setup() { 
} 
 
void loop() {
  if (msec >= SAMPLING_RATE) {
    msec = 0;
    
    // Read analog values
    for (int i = 0; i < ANA_NB; i++) {
      anaCurrentValues[i] = (int) analogRead(anaPins[i])  / 8 ;
    }

    // Then send MIDI Control Change message
    for (int i = 0; i < ANA_NB; i++) {
      if (anaCurrentValues[i] != anaPreviousValues[i]) { // Check if current value is different from previous one
        usbMIDI.sendControlChange(i+1, anaCurrentValues[i], MIDI_CHANNEL);
        anaPreviousValues[i] = anaCurrentValues[i];
      }
    }   
  }
 
  while (usbMIDI.read()) {
    // Discard incoming MIDI messages.
  }
}
